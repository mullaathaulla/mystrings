function string4(obj){
    let fullName="";
    if(obj.first_name){
        let a=obj.first_name.toLowerCase();
        fullName+=a.replace(a[0],a.charAt(0).toUpperCase())+" ";
    }
    if(obj.middle_name){
        let b=obj.middle_name.toLowerCase();
        fullName+=b.replace(b[0],b.charAt(0).toUpperCase())+" ";
    }
    if(obj.last_name){
        let c=obj.last_name.toLowerCase();
        fullName+=c.replace(c[0],c.charAt(0).toUpperCase());
    }
    return fullName;
}

module.exports=string4;