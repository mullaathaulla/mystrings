function string1(str){
    str=str.replace('$','')
    let finalString="";
    for(let i=0;i<str.length;i++){
        if(str[i]=='-' || (str[i]>="0" && str[i]<="9") || str[i]=='.'){
            finalString+=str[i];
        }
        else{
            return "0";
        }
    }
    return finalString;
}

module.exports=string1;